def is_strictly_ascending(num):
    digits = list(str(num))
    return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))

def prev_number(num):
    while True:
        num -= 1
        if is_strictly_ascending(num):
           return num

def kth_prev_reading(k: int, num: int):
    for i in range(k):
        num = prev_number(num)
        i -= 1
    return num

print(kth_prev_reading(3, 123))
print(kth_prev_reading(5, 134))   

