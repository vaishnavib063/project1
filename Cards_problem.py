import random

def initialize_decks():
    return list(range(1, 14)), list(range(1, 14)), list(range(1, 14))

def draw_random_card(deck):
    card = random.choice(deck)
    deck.remove(card)
    return card

def get_human_card(deck):
    while True:
        card = input("Choose a card to play: ")
        if card.isdigit():
            card = int(card)
            if card in deck:
                return card

def get_computer_card(deck):
    return random.choice(deck)

def determine_winner(human_card, computer_card, diamond_card):
    if human_card > computer_card:
        return "Human", diamond_card
    elif computer_card > human_card:
        return "Computer", diamond_card
    return "Tie", diamond_card / 2

def play_game():
    human_deck, computer_deck, diamond_deck = initialize_decks()
    human_score, computer_score = 0, 0

    while diamond_deck:
        diamond_card = draw_random_card(diamond_deck)
        print(f"\nDiamond card: {diamond_card}")

        human_card = get_human_card(human_deck)
        human_deck.remove(human_card)

        computer_card = get_computer_card(computer_deck)
        computer_deck.remove(computer_card)

        print(f"Human drops: {human_card}")
        print(f"Computer drops: {computer_card}")

        winner, points = determine_winner(human_card, computer_card, diamond_card)
        if winner == "Human":
            human_score += points
            print(f"Human wins the round and gets {points} points.")
        elif winner == "Computer":
            computer_score += points
            print(f"Computer wins the round and gets {points} points.")
        else:
            human_score += points
            computer_score += points
            print(f"Round tied. Both players get {points} points each.")

        print(f"Scores:\nHuman: {human_score}, Computer: {computer_score}")

    return human_score, computer_score

human_score, computer_score = play_game()
if human_score > computer_score:
    print(f"\nHuman wins with {human_score} points!")
elif computer_score > human_score:
    print(f"\nComputer wins with {computer_score} points!")
else:
    print(f"\nIt's a tie with both scoring {human_score} points!")
