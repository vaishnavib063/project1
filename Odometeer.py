# def is_strictly_ascending(num):
#     digits = list(str(num))
#     return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))
    
# def next_number(num):
#     while True:
#         num += 1
#         if is_strictly_ascending(num):
#            return num

# def next_strictly_ascending(number):
#     number = int(number)
#     return next_number(number)


# def prev_number(num):
#     while True:
#         num -= 1
#         if is_strictly_ascending(num):
#            return num 

# def prev_strictly_ascending(number):
#     number = int(number )
#     return prev_number(number)


# print(next_strictly_ascending(65))
# print(next_strictly_ascending(547))
# print(next_strictly_ascending(563))
# print(next_strictly_ascending(896))
# print(next_strictly_ascending(123))
# print(next_strictly_ascending(369))


# print(prev_strictly_ascending(65))
# print(prev_strictly_ascending(547))
# print(prev_strictly_ascending(563))
# print(prev_strictly_ascending(896))
# print(prev_strictly_ascending(123))
# print(prev_strictly_ascending(369))

def is_strictly_ascending(num):
    digits = list(str(num))
    return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))

def next_number(num):
    while True:
        num -= 1
        if is_strictly_ascending(num):
           return num

def kth_next_reading(k: int, num: int):
    for i in range(k):
        num = next_number(num)
        i -= 1
    return num

print(kth_next_reading(3, 123))
print(kth_next_reading(5, 134))


    
