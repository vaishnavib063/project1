class ReadingSequence:
    def __init__(self, n: int):
        self.n = n

    def get_limit(self) -> tuple[int, int]:
        DIGITS = "123456789"
        k = len(str(self.n))
        return int(DIGITS[:k]), int(DIGITS[-k:])

    def is_ascending(self) -> bool:
        if self.n < 10:
            return True
        digits = str(self.n)
        for i in range(len(digits) - 1):
            if digits[i] >= digits[i + 1]:
                return False
        return True

    def next_reading(self) -> int:
        start, limit = self.get_limit()
        if self.n == limit:
            return start
        n = self.n + 1
        while not self.is_ascending():
            n += 1
        return n

    def previous_reading(self) -> int:
        start, limit = self.get_limit()
        if self.n == start:
            return limit
        n = self.n - 1
        while not self.is_ascending():
            n -= 1
        return n

    def kth_next_reading(self, k: int) -> int:
        for _ in range(k):
            self.n = self.next_reading()
        return self.n

    def kth_previous_reading(self, k: int) -> int:
        for _ in range(k):
            self.n = self.previous_reading()
        return self.n

    def distance(self, b: int) -> int:
        a = self.n
        if len(str(a)) != len(str(b)):
            return -1
        dist = 0
        while a != b:
            dist += 1
            a = self.next_reading()
        return dist


